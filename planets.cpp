#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <cstdio>
#include <iomanip>

// Calculate Orbital Paths via Euler Integration
// In C++
//
// Note: This integration method is not suitable for real orbit calculation
// You may want to use "symplectic integration" instead.

// Units are:
// Length in km
// Mass in kg
// Time in s
// Energy in kg*km^2/s^2 
// Leopold Lindenbauer, October 2018, 2019



using namespace std;

const double G = 6.674e-20;
// kg * km^3 / s^2


class coordinate{
    public:
        coordinate(double x, double y, double z) : x(x), y(y), z(z) {};
        double x, y, z;
        double length();
	    double length_squared();
        void operator+=(coordinate const &rop);
        coordinate operator-(coordinate const &rop);
        coordinate operator*(double factor) const;
        bool operator==(coordinate const &rop);
        double distance(coordinate const &target) const;
        void print();
};

class planet{
    string name;
    coordinate location;
    coordinate velocity;
    double mass;
public:
    planet(string name, double mass, coordinate location, coordinate velocity): name(name), mass(mass), location(location), velocity(velocity) {};
    void printcoords();
    double distance_to(coordinate const &target) const;
    void propagate(coordinate const &accel, double deltat);
    coordinate getcoords();
    double getmass();
    double get_ekin();
};

class universe{
    vector<pair<planet, planet> > planets;
    coordinate acceleration_at_planet(int planetnum);
    double age;
public:
    universe() { age = 0.0; };
    coordinate accel_at(coordinate target);
    void addplanet(planet);
    void print_planets();
    void propagate(double dt);
    double get_energy();
    double get_epot();
};


double coordinate::length()
{
    return sqrt(length_squared());
}

double coordinate::length_squared()
{
    return x*x + y*y + z*z;
}


void coordinate::operator+=(coordinate const &rop)
{
    x+=rop.x;
    y+=rop.y;
    z+=rop.z;
    return;
}

coordinate coordinate::operator-(coordinate const &rop)
{
    return coordinate(rop.x-x, rop.y-y, rop.z-z);
}

double coordinate::distance(coordinate const &target) const
{
    return sqrt((x-target.x)*(x-target.x)+(y-target.y)*(y-target.y)+(z-target.z)*(z-target.z));
}

coordinate coordinate::operator*(double factor) const
{
    return coordinate(x*factor, y*factor, z*factor);
}

bool coordinate::operator==(coordinate const &rop)
{
    return ((x==rop.x)&&(y==rop.y)&&(z==rop.z));
}

void coordinate::print()
{
    printf("Coord: %f\t%f\t%f\tlength\t%f\n", x, y, z, length());
    return;
}

void planet::printcoords()
{
    cout << "Planet" << setw(10) << name;
    printf("\t%+1.3e \t%+1.3e \t%+1.3e \t%+1.3e \t%+1.3e \t%+1.3e \t%+1.3e \t%+1.3e\n", location.x, location.y, location.z, velocity.x, velocity.y, velocity.z, velocity.length(), get_ekin());
    return;
}

double planet::distance_to(coordinate const &target) const
{
    return location.distance(target);
}

void planet::propagate(coordinate const &accel, double deltat)
{
    velocity += (accel * deltat);
    location += (velocity * deltat);
    return;
}

coordinate planet::getcoords()
{
    return location;
}

double planet::getmass()
{
    return mass;
}

double planet::get_ekin()
{
    return mass*velocity.length_squared()/2.0;
} 

coordinate universe::accel_at(coordinate target)
{
    coordinate retval(0,0,0);
    for (int i = 0; i < planets.size(); i++)
    {
        coordinate direction = (target - planets.at(i).first.getcoords());
        if (direction.length() == 0) {
            continue;}
        double avec = planets.at(i).first.getmass()*G/(direction.length()*direction.length()*direction.length());
        retval += (direction * avec);
    }
    return retval;
}

void universe::addplanet(planet p)
{
    planets.push_back(make_pair(p,p));
    return;
}

void universe::propagate(double dt)
{
    for (int i = 0; i < planets.size(); i++)
    {
        planets.at(i).second.propagate(accel_at(planets.at(i).first.getcoords()), dt);
    }    
    for (int i = 0; i < planets.size(); i++)
    {
        planets.at(i).first = planets.at(i).second;
    }
    age += dt;
    return;
}


void universe::print_planets()
{
    printf("Age of the Universe/Days: %f\t Energy Total (MJ): %1.6e Energy Kinetic (MJ): %1.6e\n\
            Planet\t\tx\t\ty\t\tz\t\tvx\t\tvy\t\tvz\t\tvabs\t\tekin\n", (age/(static_cast<double>(60*60*24))), get_energy(), get_epot());
    for (int i = 0; i < planets.size(); i++)
    {
        planets.at(i).first.printcoords();
    }
    cout << endl;
}

double universe::get_epot()
{
    double epot = 0.0;
    for (int i = 0; i < planets.size(); ++i)
    {
        for (int j = i+1; j < planets.size(); ++j)
        {
            double r = planets.at(i).first.distance_to(planets.at(j).first.getcoords());
            epot +=G*planets.at(i).first.getmass() * planets.at(j).first.getmass() / r;
        }
    }
    return epot;
}

double universe::get_energy()
{
    double ekin = 0;
    for (int i = 0; i < planets.size(); ++i)
    {
        ekin += planets.at(i).first.get_ekin();
    }
    return ekin - get_epot();
}

int main()
{
    coordinate jupiterloc(0, 778.0e6, 0);
    coordinate jupitervel(-13.07, 0, 0);
    planet jupiter("Jupiter", 1.8982e27, jupiterloc, jupitervel);
    
    coordinate jupitertrojanloc(-389.0e6, 674.0e6, 0);
    coordinate jupitertrojanvel(-11.31895, -6.535, 0);
    planet trojan("Trojan", 2.0e20, jupitertrojanloc, jupitertrojanvel);
    
    coordinate earthloc(147.1e6, 0, 0);
    coordinate earthvel(0, 30.29, 0);
    planet earth("Earth", 5.972e24, earthloc, earthvel);
    
    coordinate sunloc(0, 0, 0);
    coordinate sunvel(0, 0, 0);
    planet sun("Sun", 1.9885e30, sunloc, sunvel);
    
    coordinate sun2loc(2.0e10, 0, 0);
    coordinate sun2vel(0, -1, 0);
    planet sun2("Nemesis", 1.9885e30, sun2loc, sun2vel);
    
    universe uni;
    uni.addplanet(jupiter);
    uni.addplanet(trojan);
    uni.addplanet(sun);
    uni.addplanet(sun2);
    
    for(long long i = 0; i < static_cast<long long>(24)*static_cast<long long>(10000); i++)
    {
        if ((i % (24*500)) == 0) uni.print_planets();
        uni.propagate(60*60*24*50);
    }
    return 0;
}
